import configparser
config = configparser.ConfigParser()
config.read('settings.ini')

# debug
enable_debug = config.getboolean("debug", "debug_enabled")
enable_trace = config.getboolean("debug", "trace_enabled")

# hardware
hardware_revision = config.get("hardware", "revision")
adv_name = config.get("hardware", "adv_name")

