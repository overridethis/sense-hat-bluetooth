from pybleno import Bleno, BlenoPrimaryService
from characteristics import *
from constants import Constants


class ServiceRunner:
    def __init__(self, log, adv_name, hardware):
        self._log = log

        self._advName = adv_name
        self._hardware = hardware

        self._bleno = Bleno()
        self._bleno.on('accept', lambda address: self._log.debug('[ACCEPTED] {}'.format(address)))
        self._bleno.on('disconnect', lambda address: self._log.debug('[ACCEPTED] {}'.format(address)))
        self._bleno.on('stateChange', lambda state: self._on_state_change(state))
        self._bleno.on('advertisingStart', lambda error: self._on_advertising_start(error))

    def run(self):
        self._bleno.start()

    def _on_state_change(self, state):
        self._log.debug('on -> stateChange: ' + state)
        if state == 'poweredOn':
            self._bleno.startAdvertising(self._advName, [Constants.service_id])
        else:
            self._bleno.stopAdvertising()

    def _on_advertising_start(self, error):
        self._log.debug('on -> advertisingStart: ' + ('error ' + error if error else 'success'))
        if not error:
            self._bleno.setServices([
                BlenoPrimaryService({
                    'uuid': Constants.service_id,
                    'characteristics': [
                        CapabilitiesCharacteristic(Constants.characteristics.capabilities_id, self._hardware),
                        IMUSensorsCharacteristic(Constants.characteristics.imu_sensors_id, self._hardware),
                        EnvSensorsCharacteristic(Constants.characteristics.env_sensors_id, self._hardware),
                        PixelCharacteristic(Constants.characteristics.pixel_id, self._hardware),
                        PixelsCharacteristic(Constants.characteristics.pixels_1_id, self._hardware, 0, 32),
                        PixelsCharacteristic(Constants.characteristics.pixels_2_id, self._hardware, 32, 64),
                        LEDCommandCharacteristic(Constants.characteristics.led_command_id, self._hardware)
                    ]
                })
            ])
