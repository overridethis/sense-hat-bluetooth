from .read_cache_charateristic import ReadCacheCharacteristic
import json


class CapabilitiesCharacteristic(ReadCacheCharacteristic):
    def __init__(self, uuid, hardware):

        def get_capabilities():
            c = hardware.get_capabilities()
            print('[CAPABILITIES]: {}'.format(c))
            return json.dumps(c).encode(encoding='UTF8')

        ReadCacheCharacteristic.__init__(self,
                                         uuid,
                                         get_capabilities)

