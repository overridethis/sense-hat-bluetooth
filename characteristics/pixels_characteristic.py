from .read_cache_charateristic import ReadCacheCharacteristic
from pybleno import Characteristic
import json


class PixelsCharacteristic(ReadCacheCharacteristic):
    def __init__(self, uuid, hardware, min_index, max_index):
        self._hardware = hardware
        ReadCacheCharacteristic.__init__(self,
                                         uuid,
                                         lambda: self._get_payload(min_index, max_index),
                                         ['read', 'write'])
        self._min = min_index
        self._max = max_index

    # @staticmethod
    # def _hex_to_rgb(value):
    #     value = value.lstrip('#')
    #     lv = len(value)
    #     return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
    #
    # @staticmethod
    # def _rgb_to_hex(rgb):
    #     return '#%02x%02x%02x' % (rgb[0], rgb[1], rgb[2])

    def _get_payload(self, min_index, max_index):
        pixels = self._hardware.get_pixels()
        print('[PIXELS] Data: {}'.format(pixels))

        # as_flat_array = [h for pixel in pixels for h in pixel]
        # rsp = array.array('B', as_flat_array)
        # print('[PIXELS] Min: {}, Max: {}, Bytes: {}'.format(min_index, max_index, len(rsp)))
        # return rsp

        rsp = pixels[min_index: max_index]
        j = json.dumps(rsp).encode(encoding='UTF8')
        print('[PIXELS: Payload] Min: {}, Max: {}, Bytes: {}'.format(min_index, max_index, len(j)))
        return j

    def onWriteRequest(self, data, offset, without_response, callback):
        raw = data.decode(encoding='UTF8')
        j = json.loads(raw)

        pixels = self._hardware.get_pixels()
        offset = 0 if self._min == 0 else self._max - 1
        for i in range(self._min, self._max):
            pixels[i] = j["pixel_list"][i - offset]

        self._hardware.set_pixels(pixels)
        callback(Characteristic.RESULT_SUCCESS)
