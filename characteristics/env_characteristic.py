from .read_cache_charateristic import ReadCacheCharacteristic
import json


class EnvSensorsCharacteristic(ReadCacheCharacteristic):
    def __init__(self, uuid, hardware):
        self._hardware = hardware
        ReadCacheCharacteristic.__init__(self, uuid, self._get_payload)

    def _get_payload(self):
        rsp = self._hardware.get_env()
        j = json.dumps(rsp).encode(encoding='UTF8')
        return j
