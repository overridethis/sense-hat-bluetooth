from pybleno import Characteristic
import json


class LEDCommandCharacteristic(Characteristic):
    def __init__(self, uuid, hardware):
        self._hardware = hardware
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['write'],
            'value': None
        })

        self._actions = {
            "clear": lambda: self._hardware.led_clear(),
            "fliph": lambda: self._hardware.led_flip_h(),
            "flipv": lambda: self._hardware.led_flip_v(),
            "rotate": lambda ang: self._hardware.led_rotate(ang),
            "message": lambda msg: self._hardware.led_show_message(msg),
        }

    def onWriteRequest(self, data, offset, without_response, callback):
        raw = data.decode(encoding='UTF8')
        j = json.loads(raw)
        command_type = j["commandType"].lower()

        if command_type in ['rotate', 'message']:
            args = j["args"]
            self._actions[command_type](args)
        else:
            self._actions[command_type]()

        callback(Characteristic.RESULT_SUCCESS)


    
