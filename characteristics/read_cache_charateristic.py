from pybleno import Characteristic
from sense_hat import SenseHat

hat = SenseHat()


class ReadCacheCharacteristic(Characteristic):
    def __init__(self, uuid, payload_callback, properties=['read']):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': properties,
            'value': None
        })

        self._payload_callback = payload_callback
        self._update_cache()
        self._updateValueCallback = None

    def onReadRequest(self, offset, callback):
        if offset == 0:
            self._update_cache()
        sample = self._sample(self._cached, offset)
        callback(Characteristic.RESULT_SUCCESS, sample)

    @staticmethod
    def _sample(selection, offset=0, limit=None):
        return selection[offset:(limit + offset if limit is not None else None)]

    def _update_cache(self):
        self._cached = self._payload_callback()
