from pybleno import Characteristic
import json


class PixelCharacteristic(Characteristic):
    def __init__(self, uuid, hardware):
        self._hardware = hardware
        Characteristic.__init__(self, {
                                'uuid': uuid,
                                'properties': ['write'],
                                'value': None})

    def onWriteRequest(self, data, offset, without_response, callback):
        raw = data.decode(encoding='UTF8')
        j = json.loads(raw)
        self._hardware.set_pixel((j['x'], j['y']), (j['r'], j['g'], j['b']))
        callback(Characteristic.RESULT_SUCCESS)


