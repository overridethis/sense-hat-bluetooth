
# Run As Service

Create a file using the command as shown below:

```
sudo nano /etc/systemd/system/sense-hat-bluetooth.service
```

Add in the following text

```
[Unit]
Description=SenseHat Bluetooth Services
After=bluetooth.target hciuart.service

[Service]
Type=simple
User=root
Group=root
WorkingDirectory=/home/pi/projects/sense-hat-bluetooth
ExecStart=/bin/sh /home/pi/projects/sense-hat-bluetooth/run.sh

[Install]
WantedBy=multi-user.target
```

Adjusts all paths to match your environment.

```
sudo systemctl daemon-reload
sudo systemctl enable sense-hat-bluetooth.service
sudo reboot
```
