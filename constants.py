# Service ID.
HAT_SERVICE_ID      = '5fd1c005-492c-4f33-87d3-a233ccf3c6e0'

# Characteristic ID.
ENV_SENSOR_C_ID     = '5fd1c005-492c-4f33-87d3-a233ccf3c6e1'
IMU_SENSOR_C_ID     = '5fd1c005-492c-4f33-87d3-a233ccf3c6e2'
LED_COMMAND_C_ID    = '5fd1c005-492c-4f33-87d3-a233ccf3c6e3'
PIXEL_C_ID          = '5fd1c005-492c-4f33-87d3-a233ccf3c6e4'

PIXELS_C_1_ID         = '5fd1c005-492c-4f33-87d3-a233ccf3c6f0'
PIXELS_C_2_ID         = '5fd1c005-492c-4f33-87d3-a233ccf3c6f1'

CAPABLITIES_C_ID      = '5fd1c005-492c-4f33-87d3-a233ccf3c6c1'


class CharConstants:
    def __init__(self):
        self.env_sensors_id = ENV_SENSOR_C_ID.replace('-', '').lower()
        self.imu_sensors_id = IMU_SENSOR_C_ID.replace('-', '').lower()
        self.led_command_id = LED_COMMAND_C_ID.replace('-', '').lower()
        self.pixel_id = PIXEL_C_ID.replace('-', '').lower()
        self.pixels_1_id = PIXELS_C_1_ID.replace('-', '').lower()
        self.pixels_2_id = PIXELS_C_2_ID.replace('-', '').lower()
        self.capabilities_id = CAPABLITIES_C_ID.replace('-','').lower()


class Constants:
    service_id = HAT_SERVICE_ID.replace('-', '').lower()
    characteristics = CharConstants()
