from sense_hat import SenseHat
from typing import Text, Tuple


class SenseHatDevice:
    def __init__(self):
        self._hat = SenseHat()

    def led_clear(self):
        self._hat.clear()

    def led_flip_h(self):
        self._hat.flip_h()

    def led_flip_v(self):
        self._hat.flip_v()

    def led_rotate(self, angle: int):
        self._hat.set_rotation(angle)

    def show_message(self, msg: Text):
        self._hat.show_message(msg)

    def set_pixel(self, coord: Tuple[int, int], color: Tuple[int, int, int]):
        self._hat.set_pixel(coord[0], coord[1], (color[0], color[1], color[2]))

    def get_pixels(self):
        return self._hat.get_pixels()

    def get_imu(self):
        rsp = {
            'orientation': self._hat.get_orientation(),
            'gyroscope': self._hat.get_gyroscope(),
            'accelerometer': self._hat.get_accelerometer(),
            'compass': self._hat.get_compass()
        }
        return rsp

    def get_env(self):
        rsp = {
            'temperature': self._hat.get_temperature(),
            'humidity': self._hat.get_humidity(),
            'pressure': self._hat.get_pressure()
        }
        return rsp

    @staticmethod
    def get_capabilities():
        rsp = {
            'env': True,
            'env_options': {
                'temperature': True,
                'humidity': True,
                'pressure': True
            },
            'led': True,
            'led_options': {
                'width': 8,
                'height': 8
            },
            'imu': True,
            'imu_options': {
                'orientation': True,
                'gyroscope': True,
                'accelerometer': True,
                'compass': True
            }
        }
        return rsp

