from typing import Tuple, Text
from .sense_hat_device import SenseHatDevice


class Device:
    def __init__(self, config):
        self._config = config;
        self._hardware = SenseHatDevice()

    def led_clear(self):
        self._hardware.led_clear()

    def led_flip_h(self):
        self._hardware.led_flip_h()

    def led_flip_v(self):
        self._hardware.led_flip_v()

    def led_rotate(self, angle: int):
        self._hardware.led_rotate(angle)

    def led_show_message(self, msg: Text):
        self._hardware.show_message(msg)

    def set_pixel(self, coord: Tuple[int, int], color: Tuple[int, int, int]):
        self._hardware.set_pixel(coord, color)

    def get_pixels(self):
        return self._hardware.get_pixels()

    def get_imu(self):
        return self._hardware.get_imu()

    def get_env(self):
        return self._hardware.get_env()

    def get_capabilities(self):
        return self._hardware.get_capabilities()

    def get_revision(self):
        return self._config.hardware_revision


