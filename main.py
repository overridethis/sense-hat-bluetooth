#!/usr/bin/python3
from threading import Thread
import time

# import configuration
import app_config as CONFIG

# hardware
from service_runner import ServiceRunner
from sensors import Device

# setup logging.
import app_logger as LOG
LOG.enableTrace(True)
LOG.warning('BlueTooth LE - Matrix Fun!')


class Main:
    def __init__(self, log, config):
        self._log = log

        # run services.
        hardware = Device(config)
        runner = ServiceRunner(log, config.adv_name, hardware)
        runner.run()

        def log_env_readings():
            while True:
                log.warning(hardware.get_env())
                time.sleep(5)

        Thread(target=log_env_readings()).start()


if __name__ == "__main__":
    Main(LOG, CONFIG)

LOG.warning('terminated.')
